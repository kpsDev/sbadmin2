# SB Admin 2 MVC

## Live demo

http://sbadmin2.azurewebsites.net/

## Credits

This template is based on the Start Bootstrap SB Admin 2 template and adapted for MVC. For more information
on the original see [Startbootstrap.com](http://startbootstrap.com/template-overviews/sb-admin-2/).

Modified By: Warren Caulton
* https://twitter.com/warrencaulton

Since I do not have the authority to distribute Microsoft Software. 
The way to use this project is to first create a new MVC 4 project using Visual Studio.  I have tested this in VS2012 and VS2013.
Run the project to confirm that is is created correctly.
Get a copy of this code and copy the Folders from this project onto the MVC project -use the solution explorer window in VS and the Windows Explorer.
Build and test after the copy is complete.



## Features:

  * Added reference to [spin.js](http://fgnass.github.io/spin.js/).

## Not done (suggestions?)

  * Add WebAPI examples for the data used in the tables and charts.

## This below copyright and license are as provided by the original repository

Start Bootstrap was created by and is maintained by **David Miller**, Managing Parter at [Iron Summit Media Strategies](http://www.ironsummitmedia.com/).

* https://twitter.com/davidmillerskt
* https://github.com/davidtmiller

Start Bootstrap is based on the [Bootstrap](http://getbootstrap.com/) framework created by [Mark Otto](https://twitter.com/mdo) and [Jacob Thorton](https://twitter.com/fat).

## Copyright and License

Copyright 2013-2015 Iron Summit Media Strategies, LLC. 
Code released under the [Apache 2.0](https://github.com/IronSummitMedia/startbootstrap-sb-admin-2/blob/gh-pages/LICENSE) license.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the
License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific
language governing permissions and limitations under the License.